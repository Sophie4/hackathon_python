from flask import Flask, request
from yahoo_fin import stock_info as si
from datetime import datetime
from dateutil.relativedelta import relativedelta

app = Flask(__name__)

@app.route('/trade_decision', methods=['GET'])

def getData():
    
    stocks = request.args['stocks'].strip('][').split(',')
    print(stocks)
    output = ''
    
    for element in stocks:
            
        trade_data = si.get_data('aapl')[datetime.now().date()-relativedelta(months=2):datetime.now().date()]
        
        trade_data['30d mavg'] = trade_data['close'].rolling(window=30).mean()
        trade_data['30d std'] = trade_data['close'].rolling(window=30).std()
        trade_data['upper band'] = trade_data['30d mavg'] + (trade_data['30d std'] * 2)
        trade_data['lower band'] = trade_data['30d mavg'] - (trade_data['30d std'] * 2)
        
        trade_data.dropna(inplace=True)
        
        latest_data = trade_data.tail(1)
        close_price = latest_data['adjclose'][0]
        upper_band = latest_data['upper band'][0]
        lower_band = latest_data['lower band'][0]
        
        if close_price < lower_band:
            action = 'BUY'
        elif close_price > upper_band:
            action = 'SELL'
        else:
            action = 'HOLD'
            
        output = output + element.upper() + ': ' + action + '<br/>'
        
    return output

if __name__ == '__main__':
    app.run(debug=True)