from flask import Flask, request
from yahoo_fin import stock_info as si

app = Flask(__name__)

@app.route('/trade_data', methods=['GET'])

def getData():
    
    if (request.args['historical'] is None or request.args['stocks'] is None):
        return 'Ensure all parameters are provided'
    else:
        print('NOT EMPTY')
        historical = request.args['historical']
        stocks = request.args['stocks'].strip('][').split(',')
        
        print(historical)
        print(stocks)
        
        output = ''
        
        if historical == 'True':
            print('HISTORICAL')
            dates = request.args.get('dates').strip('][').split(',')
            for element in stocks:
                output = output + element.upper() + '<br/>'
                output += '-------------------------------------------------------------------------------------------<br/>'
                output += si.get_data(element.lower()).drop(columns=['ticker'])[dates[0]:dates[1]].to_json()
                output += '<br/>-------------------------------------------------------------------------------------------<br/><br/>'
        else:
            print('LIVE')
            for element in stocks:
                output = output + element.upper() + ': ' + '$' + str(round(si.get_live_price(element), 2))
                output = output + "<br/>"
                
        return output

if __name__ == '__main__':
    app.run(debug=True)